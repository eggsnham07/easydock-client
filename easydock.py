#!/usr/bin/python3

"""
##########################################
################ APP INFO ################
##########################################
#### Version: 1.0.2 ######################
#### Config File: settings.py ############
##########################################
"""

from subprocess import Popen
from settings import config
import RPi.GPIO as gpio
import datetime as date
from time import sleep
from lib import I2C
import easygui
import os

buttons = [
    {"pin": 17, "pressed": False, "id": "right-right"},
    {"pin": 27, "pressed": False, "id": "right-left"},
    {"pin": 22, "pressed": False, "id": "middle"},
    {"pin": 5, "pressed": False, "id": "left-right"},
    {"pin": 6, "pressed": False, "id": "left-left"}
]

quit_button = {
    "pin": 13,
    "pressed": False
}

led = {
    "pin": 14
}

buzzer = {
    "pin": 19
}

def setup():
    iplen = 0
    for i in config["external_ip"].split("."):
        iplen += 1
    
    gpio.cleanup()

    for button in buttons:
        gpio.setup(button["pin"], gpio.IN, pull_up_down=gpio.PUD_DOWN)
    
    gpio.setup(led["pin"], gpio.OUT)
    gpio.setup(buzzer["pin"], gpio.OUT)

    gpio.setup(quit_button["pin"], gpio.IN, pull_up_down=gpio.PUD_DOWN)
    
    gpio.output(led["pin"], gpio.HIGH)

    if config["I2C"]["enabled"]:
        if config["I2C"]["display"] == "ip":
            if config["I2C"]["ip_display"] == "external":
                I2C.print("E" + config["external_ip"].split(".")[iplen-1])
            else:
                I2C.print("L" + I2C.get_ip()[3])

        if config["I2C"]["display"] == "time":
            devnull = open(os.devnull, "wb")
            Popen(["nohup", "python3", "/usr/bin/easydocklib/time.py"])

    

setup()

def beep(pin:int):
    gpio.output(pin, gpio.HIGH)
    sleep(0.1)
    gpio.output(pin, gpio.LOW)
    sleep(0.1)
    
def end():
    gpio.output(led["pin"], gpio.LOW)
    i = 0
            
    while i < config["exit-beeps"]:
        beep(buzzer["pin"])
        i += 1

    gpio.cleanup()
    I2C.clear()    
    exit(0)

def button_press_local():
    if gpio.input(quit_button["pin"]) == gpio.HIGH:
        qt = easygui.ynbox("Are you sure you want to quit EasyBoard?", "Confirm", ("Yes", "No"))
        if qt == True:
            end()

    for button in buttons:
        devnull = open(os.devnull, "wb")
        if button["pressed"] == False and gpio.input(button["pin"]) == gpio.HIGH:
            print("Launching " + config["binds"][button["id"]])
            button["pressed"] = True
            Popen(["nohup", config["binds"][button["id"]]], stdout=devnull, stderr=devnull)
        elif button["pressed"] == True and gpio.input(button["pin"]) == gpio.LOW:
            button["pressed"] = False

def button_press_server():
    for button in buttons:
        devnull = open(os.devnull, "wb")
        if button["pressed"] == False and gpio.input(button["pin"]) == gpio.HIGH:
            print("Attempt launch on external device")
            button["pressed"] = True
            Popen(["nohup", "curl", "http://" + config["external_ip"] + ":2516/buttons/" + button["id"].replace("-", "_")], stdout=devnull, stderr=devnull)
        elif button["pressed"] == True and gpio.input(button["pin"]) == gpio.LOW:
            button["pressed"] = False


while True:
    if config["mode"] == "local":
        button_press_local()
    elif config["mode"] == "external":
        button_press_server()
