# Current config will:
# - Launch apps locally
# - Disable 7 segment I2C display
# - Display local IP if I2C is enabled
# - Beep 3 times when exiting program

config = {
    "binds": {
        "left-left": "/usr/bin/thonny",
        "left-right": "lxterminal",
        "middle": "minecraft-pi",
        "right-left": "pcmanfm",
        "right-right": "chromium-browser"
    },
    "exit-beeps": 3,
    "mode": "local",
    "external_ip": "127.0.0.1",
    "I2C": {
        "enabled": True,
        "display": "time",
        "ip_display": "local"
    }
}
