import time
import board
import busio
import subprocess
from adafruit_ht16k33 import segments

def get_ip():
    ipbytes = subprocess.check_output(["hostname", "-I"])
    ip = ''.join(map(chr, ipbytes)).replace(" \n", '').split(".")
    return(ip)

i2c = busio.I2C(board.SCL, board.SDA)

display = segments.Seg7x4(i2c)

display.fill(0)

def display_ip(ip, timeout):
    for i in ip:
        display.print(i)
        time.sleep(timeout)
        display.fill(0)

def print(string:str):
    display.print(string)

def clear():
    display.fill(0)
