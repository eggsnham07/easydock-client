from lib import I2C
from time import sleep
from datetime import datetime

while True:
    minutes = str(datetime.now().minute)

    if minutes.__len__() == 1:
        minutes = minutes.replace("1", "01").replace("2", "02").replace("3", "03").replace("4", "04").replace("5", "05").replace("6", "06").replace("7", "07").replace("8", "08").replace("9", "09")

    time = str(datetime.now().hour) + minutes
    I2C.print(time)
    sleep(1)